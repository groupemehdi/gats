
#on part de l'image officielle de nodejs
FROM node as build

WORKDIR /app

# on installe npm
COPY package.json .
RUN npm update
RUN npm install
# de façon a pouvoir installer gatsby
RUN npm i -g gatsby-cli
RUN /usr/local/bin/npm install
RUN gatsby build
RUN pwd
# copy the whole source folder(the dir is relative to the Dockerfile
COPY /app/public /usr/local/apache2/temp

FROM httpd
COPY --from=build /usr/local/apache2/temp /usr/local/apache2/htdocs

#COPY /scriptApache.sh /app/scriptApache.sh
#ENTRYPOINT /app/scriptApache.sh ; /bin/bash

